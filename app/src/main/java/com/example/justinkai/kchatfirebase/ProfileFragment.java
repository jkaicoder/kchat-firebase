package com.example.justinkai.kchatfirebase;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private Button btnSignOut, btnDelete;
    private FirebaseAuth mAuth;
    private TextView tvName;
    private CircleImageView imvAvatar;
    private FirebaseFirestore mFirestore;

    private String mUserID;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        mUserID = mAuth.getCurrentUser().getUid();
        mFirestore.collection("users").document(mUserID).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String userName = documentSnapshot.getString("name");
                String userImage = documentSnapshot.getString("image");

                tvName.setText(userName);
                RequestOptions placeholderOption = new RequestOptions();
                placeholderOption.placeholder(R.drawable.man);
                Glide.with(container.getContext()).setDefaultRequestOptions(placeholderOption).load(userImage).into(imvAvatar);
            }
        });

        tvName = view.findViewById(R.id.tvName);
        imvAvatar = view.findViewById(R.id.imvAvatar);
        btnSignOut = view.findViewById(R.id.btnSignOut);
        btnDelete = view.findViewById(R.id.btnDelete);

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent intent = new Intent(container.getContext(), SignInActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete Account")
                        .setMessage("Are you sure you want to delete this account?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mFirestore.collection("users").document(mAuth.getUid()).delete();
                                mAuth.getCurrentUser().delete();
                                goToLogin();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
        return view;
    }

    private void goToLogin() {
        startActivity(new Intent(getContext(), SignInActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
