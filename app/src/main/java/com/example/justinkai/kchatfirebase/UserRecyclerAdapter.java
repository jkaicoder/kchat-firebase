package com.example.justinkai.kchatfirebase;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.RecyclerViewHolder> {
    private List<Users> usersList = new ArrayList<>();
    private Context context;
    private Dialog myDialog;


    public UserRecyclerAdapter(Context context, List<Users> usersList){
        this.context = context;
        this.usersList = usersList;
    }

    @NonNull
    @Override
    public UserRecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.user_item, viewGroup,false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserRecyclerAdapter.RecyclerViewHolder recyclerViewHolder, int i) {
        recyclerViewHolder.tvName.setText(usersList.get(i).getName());
        CircleImageView userImage = recyclerViewHolder.imvAvatar;
        Glide.with(context).load(usersList.get(i).getImage()).into(userImage);

        final String userID = usersList.get(i).getId();

        recyclerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogProfile();
            }
        });

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        CircleImageView imvAvatar;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            imvAvatar = itemView.findViewById(R.id.imvAvatar);
        }
    }

    public void showDialogProfile(){
        myDialog = new Dialog(context);
        myDialog.setContentView(R.layout.dialog_profile);
        myDialog.setTitle("My Custom Dialog");
        myDialog.show();
    }
}
