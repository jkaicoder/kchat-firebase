package com.example.justinkai.kchatfirebase;

public class Users {
    private String id, name, image;

    public Users(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public Users() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
