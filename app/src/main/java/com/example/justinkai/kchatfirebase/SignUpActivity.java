package com.example.justinkai.kchatfirebase;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    private EditText edtEmail, edtPassword, edtConfirmPassword, edtName;
    private Button btnRegister, btnBackLogin;
    private CircleImageView imvAvatar;
    private ProgressBar prgbRegister;
    private Intent logIntent;

    private Uri imageUri;

    private FirebaseAuth mAuth;
    private StorageReference mReferenceRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mReferenceRef = FirebaseStorage.getInstance().getReference().child("images");
        mAuth = FirebaseAuth.getInstance();
        final FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();


        findView();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prgbRegister.setVisibility(View.VISIBLE);
                if (imageUri != null){
                    String email = edtEmail.getText().toString();
                    String passWord = edtPassword.getText().toString();
                    String conPassWord = edtConfirmPassword.getText().toString();
                    final String name = edtName.getText().toString();

                    if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(passWord) && !TextUtils.isEmpty(conPassWord) && !TextUtils.isEmpty(name)){
                        if (passWord.equals(conPassWord)){
                            if (passWord.length() >= 6){
                                mAuth.createUserWithEmailAndPassword(email, passWord).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            final String userID = mAuth.getCurrentUser().getUid();
                                            final StorageReference userProfile = mReferenceRef.child(userID + ".jpg");
                                            userProfile.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                                    if (task.isSuccessful()){
                                                        userProfile.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                            @Override
                                                            public void onSuccess(Uri uri) {
                                                                Map<String, Object> userMap = new HashMap<>();
                                                                userMap.put("name", name);
                                                                userMap.put("image", uri.toString());

                                                                mFirestore.collection("users").document(userID).set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                    @Override
                                                                    public void onSuccess(Void aVoid) {
                                                                        goToMain();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        Toast.makeText(SignUpActivity.this, "Error: " + task.getException(), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(SignUpActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                                            prgbRegister.setVisibility(View.INVISIBLE);
                                        }
                                    }
                                });
                            } else{
                                Toast.makeText(SignUpActivity.this, "The password require least 6 characters", Toast.LENGTH_SHORT).show();
                                prgbRegister.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            Toast.makeText(SignUpActivity.this,  "The two password fields did not match", Toast.LENGTH_SHORT).show();
                            prgbRegister.setVisibility(View.INVISIBLE);
                        }
                    } else{
                        Toast.makeText(SignUpActivity.this, "Please verify all field ", Toast.LENGTH_SHORT).show();
                        prgbRegister.setVisibility(View.INVISIBLE);
                    }
                } else{
                    Toast.makeText(SignUpActivity.this, "Please select your avatar ", Toast.LENGTH_SHORT).show();
                    prgbRegister.setVisibility(View.INVISIBLE);
                }
            }
        });

        btnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIntent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(logIntent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        imvAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }

    private void goToMain() {
        Intent mainIntent = new Intent(SignUpActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    private void findView() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtName = findViewById(R.id.edtName);

        btnBackLogin = findViewById(R.id.btnBackLogin);
        btnRegister = findViewById(R.id.btnRegister);

        imvAvatar = findViewById(R.id.imvAvatar);
        prgbRegister = findViewById(R.id.prsbRegister);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE){
            imageUri = data.getData();
            imvAvatar.setImageURI(imageUri);
        }
    }
}
