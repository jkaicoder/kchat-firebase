package com.example.justinkai.kchatfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity {

    private EditText edtEmail, edtPassword;
    private Button btnLogin, btnRegister;
    private Intent regIntent;
    private ProgressBar prgbLogin;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mAuth = FirebaseAuth.getInstance();
        findView();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regIntent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(regIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prgbLogin.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.INVISIBLE);
                String email = edtEmail.getText().toString();
                String passWord = edtPassword.getText().toString();

                if (email.isEmpty() && passWord.isEmpty()){
                    prgbLogin.setVisibility(View.INVISIBLE);
                    btnLogin.setVisibility(View.VISIBLE);
                    nofify("All filed not empty");
                } else {
                    mAuth.signInWithEmailAndPassword(email, passWord).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                goToMain();
                            } else {
                                nofify(task.getException().toString());
                                prgbLogin.setVisibility(View.INVISIBLE);
                                btnLogin.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        });
    }

    private void goToMain() {
        startActivity(new Intent(SignInActivity.this, MainActivity.class));
        finish();
    }

    private void nofify(String mess) {
        Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_SHORT).show();
    }


    private void findView() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin =  findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
        prgbLogin = findViewById(R.id.prgbLogin);
    }
}
