package com.example.justinkai.kchatfirebase;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;


/**
 * A simple {@link Fragment} subclass.
 */
public class AllUserFragment extends Fragment {

    private RecyclerView rcvUserList;
    private List<Users> userList;

    private UserRecyclerAdapter userRecyclerAdapter;

    private FirebaseFirestore mFirestore;
    private FirebaseAuth mAuth;



    public AllUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_user, container, false);
        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        rcvUserList = view.findViewById(R.id.rcvUserList);
        userList = new ArrayList<>();

        userRecyclerAdapter = new UserRecyclerAdapter(container.getContext(), userList);

        rcvUserList.setHasFixedSize(true);
        rcvUserList.setLayoutManager(new LinearLayoutManager(container.getContext()));
        rcvUserList.setAdapter(userRecyclerAdapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        userList.clear();
        final String currentUserID = mAuth.getCurrentUser().getUid();
        mFirestore.collection("users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()){
                    if (doc.getType() == DocumentChange.Type.ADDED){
                        String userID = doc.getDocument().getId();
                        if (!currentUserID.equals(userID)){
                            Users users = doc.getDocument().toObject(Users.class);
                            users.setId(userID);
                            userList.add(users);
                        }
                        userRecyclerAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }



}
