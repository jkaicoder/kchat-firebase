package com.example.justinkai.kchatfirebase;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private TextView tvProfile, tvAllUser, tvFriends, tvNotification;
    private ViewPager vpMain;

    private PagerViewAdapter pagerViewAdapter;

    private FirebaseAuth mAuth;

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser  = mAuth.getCurrentUser();
        if (currentUser == null){
            goToLogin();
        }
    }

    private void goToLogin() {
        Intent intent = new Intent(MainActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        findView();
        vpMain.setOffscreenPageLimit(3);
        pagerViewAdapter = new PagerViewAdapter(getSupportFragmentManager());
        vpMain.setAdapter(pagerViewAdapter);

        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(0);
            }
        });

        tvAllUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(1);
            }
        });

        tvFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(2);
            }
        });

        tvNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(3);
            }
        });

        vpMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void changeTabs(int position) {
        if (position == 0){
            tvProfile.setTextColor(getColor(R.color.white));
            tvProfile.setTextSize(20);

            tvAllUser.setTextColor(getColor(R.color.grayLite));
            tvAllUser.setTextSize(14);

            tvFriends.setTextColor(getColor(R.color.grayLite));
            tvFriends.setTextSize(14);

            tvNotification.setTextColor(getColor(R.color.grayLite));
            tvNotification.setTextSize(14);
        } else if (position == 1){
            tvProfile.setTextColor(getColor(R.color.grayLite));
            tvProfile.setTextSize(14);

            tvAllUser.setTextColor(getColor(R.color.white));
            tvAllUser.setTextSize(20);

            tvFriends.setTextColor(getColor(R.color.grayLite));
            tvFriends.setTextSize(14);

            tvNotification.setTextColor(getColor(R.color.grayLite));
            tvNotification.setTextSize(14);
        } else if (position == 2){
            tvProfile.setTextColor(getColor(R.color.grayLite));
            tvProfile.setTextSize(14);

            tvAllUser.setTextColor(getColor(R.color.grayLite));
            tvAllUser.setTextSize(14);

            tvFriends.setTextColor(getColor(R.color.white));
            tvFriends.setTextSize(20);

            tvNotification.setTextColor(getColor(R.color.grayLite));
            tvNotification.setTextSize(14);
        } else{
            tvProfile.setTextColor(getColor(R.color.grayLite));
            tvProfile.setTextSize(14);

            tvAllUser.setTextColor(getColor(R.color.grayLite));
            tvAllUser.setTextSize(14);

            tvFriends.setTextColor(getColor(R.color.grayLite));
            tvFriends.setTextSize(14);

            tvNotification.setTextColor(getColor(R.color.white));
            tvNotification.setTextSize(20);
        }
    }

    private void findView() {
        tvProfile = findViewById(R.id.tvProfile);
        tvAllUser = findViewById(R.id.tvAllUser);
        tvNotification = findViewById(R.id.tvNotification);
        tvFriends = findViewById(R.id.tvFriends);
        vpMain = findViewById(R.id.vpMain);

    }
}
